# Official language image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/python/tags/
image: python:3.8

variables:
  # Change pip's cache directory to be inside the project directory since we can
  # only cache local items.
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  # Ignore pipenv venv warning (we use venv to allow having this in a pipeline)
  PIPENV_VERBOSITY: -1
  # OPENBLAS workaround for pipeline core detection
  # Workaround needed when using some numpy functionality
  # OPENBLAS_CORETYPE: ARM64
  # OPENBLAS_VERBOSE: 2

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
# Uncomment when having multiple stages to allow for reuse
# cache:
#   paths:
#     - .cache/pip
#     - venv/

# Per default stages are executed in sequence and
# all jobs in the stages run in parallel
stages:
  - test
  - deploy

before_script:
  # These commands will be executed before all jobs
  - python -V  # Print out python version for debugging
  
test:
  # This jobs executes the tests
  # The results are stored in the 'htmlcov' directory, which is
  # made available to later stages via 'paths:' option
  stage: test
  script:
    - pip install virtualenv
    - virtualenv venv
    - source venv/bin/activate
    - pip install pipenv
    - pipenv install
    - pytest -v --cov=src --cov-report html --cov-report term
  artifacts:
    paths:
      - htmlcov/

code_style:
  stage: test
  script:
    - pip install pycodestyle
    - bash stylecheck.sh
  allow_failure: true

doc:
  # This job generates the API documentation
  # The results are stored in the 'html' directory, which is
  # made available to later stages via 'paths:' option
  stage: test
  script:
    - pip install virtualenv
    - virtualenv venv
    - source venv/bin/activate
    - pip install pipenv
    - pipenv install
    - pdoc --html --force ./src/dojo
  artifacts:
    paths:
      - html/

pages:
  # This job publishes the pipeline results (coverage, documentation) on the gitlab pages
  # As 'htmlcov' and 'html' are created in the test stage we just need to copy the data here...
  stage: deploy
  script:
    - mkdir -p public/coverage
    - mkdir -p public/doc
    - ls -la htmlcov
    - cp -r htmlcov/* public/coverage
    - ls -la public/coverage/
    - cp -r html/dojo* public/doc
  artifacts:
    paths:
      - public
  rules:
    # We do not want commits on non-main branches to be published!
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
