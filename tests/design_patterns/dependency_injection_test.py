"""
Tests for dependency injection examples
"""
import pytest
from src.dojo.design_patterns.dependency_injection import ConstructorInjection, SetterInjection


def test_constructor_dependency():
    """
    Construct the client with different types of dependencies to showcase flexibility
    """
    assert ConstructorInjection('dependency').call_dependency() == 'str'
    assert ConstructorInjection([]).call_dependency() == 'list'


def test_constructor_dependency_with_none():
    """
    Construct the client with different types of dependencies to showcase flexibility
    """
    with pytest.raises(RuntimeError):
        ConstructorInjection(None)


def test_setter_injection_with_none():
    """
    In this case we are not forced to inject a dependency on creation.
    It will then use the default dependency from the lazy initialisation.
    """
    si = SetterInjection()
    # No service configured
    assert si.dependency is None
    # But we can call it safely
    assert si.call_dependency() == 'list'
    # Magically it is set now
    assert isinstance(si.dependency, list)


def test_setter_injection_change_at_runtime():
    """
    In this case we are not forced to inject a dependency on creation.
    It will then use the default dependency from the lazy initialisation.
    """
    si = SetterInjection()

    # Set a service:
    si.set_dependency('test')
    assert si.call_dependency() == 'str'
    # And change it at runtime:
    si.set_dependency({})
    assert si.call_dependency() == 'dict'
