#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
# Mocking example for user interaction

In unit testing you do not want to interact with your program manually,
but rather have an automated and pre-defined scenario.
To avoid the need of typing in user input and in order to be able to test
output send to the command line we use a mock.

pytest has a simple mocking interface for that.
The mocker object is passed to the method as a fixture, and can be used without initialisation.
There is also the possibility to create the mocker explicitly, but as long as you just want to replace method calls,
the fixture approach is good enough and saves a lot of code.
See [pytest mocking API](https://pypi.org/project/pytest-mock/) for details.

A short introduction to mocking can be found in this blog post: https://changhsinlee.com/pytest-mock/

@author: hoelken
"""
from unittest.mock import MagicMock, call
from src.dojo.testing_examples.interface import write_to_system_out, get_user_input


def test_simulate_user_input(mocker) -> None:
    """
    Minimal example on how to simulate user input
    """
    # As usual, we just define a new return value und bypass the input call completely
    mocker.patch('builtins.input', return_value='Test')
    assert get_user_input() == 'Test'

    # Important is that we define the desired behaviour prior to the call in our test code.
    mocker.patch('builtins.input', return_value=123)
    assert get_user_input() == '123'


def test_assert_printed_message(mocker) -> None:
    """
    Minimal example on how to assert the message that was printed on standard out
    """
    # First setup the mocker to intercept calls to print
    # Note that from this line on no print statement will make it to the console anymore
    # for the rest of this test method
    printer = mocker.patch('builtins.print')

    # Now we call the method to print something to the std out
    write_to_system_out()

    # Check printer was called
    assert printer.call_count == 1
    # Check message that was sent to the printer
    printer.assert_called_with('Hello World')


def test_assert_multiple_messages(mocker) -> None:
    """
    Example for asserting multiple calls
    """
    printer = mocker.patch('builtins.print')

    # Now we call the method to print something to the std out
    write_to_system_out("A")
    write_to_system_out("B")
    write_to_system_out("C")

    # Check printer was called
    assert printer.call_count == 3

    # Check message that was sent to the printer
    # But this way, we can only test the last one :(
    printer.assert_called_with('C')

    # Solution A: use has calls:
    printer.assert_has_calls((call("A"), call("B"), call("C")))

    # Solution B: We use a custom assertion to check for other calls:
    # This method could also allow regex wildcards, etc.
    _assert_message_printed(printer, 'A')
    _assert_message_printed(printer, 'B')
    _assert_message_printed(printer, 'C')


def _assert_message_printed(printer: MagicMock, message: str) -> None:
    # DRY stuff out with custom assertions!
    assert call(message) in printer.mock_calls
