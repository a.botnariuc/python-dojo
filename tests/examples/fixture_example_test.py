#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
# Test fixture example

In this scenario we will use a custom pytest fixture.
fixtures can be created in their own test-resources module or directly in the test file.

@author: hoelken
"""

import pytest


@pytest.fixture(scope='module')
def some_dict() -> dict:
    """
    With the annotation this method is marked as a fixture and cann now be used in the testing methods.
    A fixture can have different scopes, to allow re-use with or without re-creation as needed:
        Function – This is the default value of the fixture scope. Fixture with function scope is executed once per
                session.
        Package – A pytest fixture with scope as package is created only once for the entire test run.
        Module – As the name indicates, a fixture function with scope as Module is created only once per module.
        Class – The fixture function is created once per class object.
    If you use the `autouse` keyword the fixture will be automagically use in all tests. However, this is usually
    not wanted as it can be hard to find out whats going on in the actual test method then.
    """
    return {'foo': 'bar', 'baz': 'qwurx'}


def test_with_fixture(some_dict) -> None:
    """
    This method can use the pytest fixture, identified by the methods name, without caring on setup etc.
    You can pass multiple fixtures to a test method, if needed.
    """
    assert type(some_dict) is dict
    assert 'foo' in some_dict.keys()
    assert 'baz' in some_dict.keys()
