#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
# Extended mocking example

In the `minimal_mocking_test.py` we had a brief look on mocking static methods in a quick way.
Sometimes you need a bit more than that. In this example we will
 - mock collaborators from different sources
 - mock entire classes
 - mock non-static instance methods

@author: hoelken
"""

import pytest
from unittest.mock import call

from src.dojo import Hal9000, Calc


def test_mock_lib_import(mocker) -> None:
    """
    The library was imported as a whole with `import time`. Therefore, we have to patch the
    method we want to bypass at the library location ('time')
    """
    mocker.patch('time.time_ns', return_value=1337)
    hal = Hal9000('Dave')
    assert hal.random() == 4 * 1337


def test_mock_lib_import_from(mocker) -> None:
    """
    In this scenario the library method was imported directly with `from time import time_ns`. Therefore,
    we have to patch it _in the importing_ module ('dojo.calculator').
    """
    mocker.patch('src.dojo.testing_examples.calculator.time_ns', return_value=1337)
    calc = Calc(42)
    assert calc.time_times_value() == 42 * 1337


def test_mock_class(mocker) -> None:
    """
    This time we want to mock a complete object.

    Unfortunately the class under test does not allow dependency injection, so we have to patch the `__init__` method
    of the collaborator class.
    """
    # Again the import was doe via 'from foo import bar', so we patch the imported location
    mock = mocker.patch('src.dojo.testing_examples.calculator.Hal9000')
    # As we have patched the object we need to retrieve the actual mocked instance via return_value
    mocked_instance = mock.return_value
    # Now for the configuration: Just define the return value
    mocked_instance.random.return_value = 42

    calc = Calc(2)
    # assert the result
    assert calc.random_division() == 21
    # and/or assert the method in the collaborator was called
    assert mocked_instance.random.called
    # If the called method would take arguments, it would also be easy th check them:
    # mocked_instance.random.assert_called_with(arg1, arg2, kwarg=arg3)


def test_mock_class_multiple_calls(mocker) -> None:
    """
    As above, but this time we want to have different results each time we call the method.
    """
    mock = mocker.patch('src.dojo.testing_examples.calculator.Hal9000')
    mocked_instance = mock.return_value
    # Now for the configuration, when we assign an iterable to the side-effect it will return the next
    # element, whenever the method is called
    mocked_instance.random.side_effect = [2, 4, 6]

    calc = Calc(2)
    assert calc.random_division() == 1
    assert calc.random_division() == 2
    assert calc.random_division() == 3
    assert mocked_instance.random.called


def test_mock_class_multiple_call_assertions(mocker) -> None:
    """
    As above, but this time we want to check the arguments a method was called with.
    """
    mock = mocker.patch('src.dojo.testing_examples.calculator.Hal9000')
    mocked_instance = mock.return_value
    # Now for the configuration, when we assign an iterable to the side-effect it will return the next
    # element, whenever the method is called
    mocked_instance.random.return_value = 42

    calc = Calc(2)
    # Call with different arguments
    calc.random_division(factor=1)
    calc.random_division(factor=2)
    calc.random_division(factor=3)
    mocked_instance.random.assert_has_calls([call(factor=1), call(factor=2), call(factor=3)])


def test_mock_instance_variables(mocker) -> None:
    """
    Sometimes you want to fine tune also the instance variables of your mocked object.
    It's as easy as the other scenarios
    """
    # We will play with Hal directly, so we have to patch it at we see it from here
    # Note the path to the mocked object! It is here!
    mock = mocker.patch('tests.examples.extended_mocking_test.Hal9000')
    # We need to get the instance to be mocked
    mocked_instance = mock.return_value
    # Now we can set the instance variables
    mocked_instance.user = 'Foo'

    hal = Hal9000('Dave')
    # Yes, we have created the object with user 'Dave#, but we have pre-defined that the user
    # on the mock should be 'Foo', so this is what we get here.
    assert hal.user == 'Foo'
    # And indeed, we are not really talking to HAL, but to our mock instead
    assert hal == mocked_instance


def test_mock_exceptional_cases(mocker) -> None:
    """
    Often you want to test how your code behaves if an exception is raised.
    Sometimes you can trigger those exceptions directly by providing evil data, but often enough you cant
    do this (without a major effort).
    When you use a mock, you can trigger exceptions easily:
    """
    mock = mocker.patch('src.dojo.testing_examples.calculator.Hal9000')
    mocked_instance = mock.return_value
    # We use the `side_effect` functionality again, and this time we use explosives:
    mocked_instance.random.side_effect = Exception('Boom!')

    calc = Calc(123)
    with pytest.raises(Exception) as excinfo:
        # This is going to hurt ;)
        calc.random_division()
    # You can even test for the exception message, if you want to
    assert ('Boom!', ) == excinfo.value.args
