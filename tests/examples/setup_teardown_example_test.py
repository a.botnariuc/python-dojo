#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
# Test class unit test example

In the setup we will create a test class. This will allow us to use
teardown and setup mechanisms. Setup and teardown comes in handy when
your test needs a special configuration, that you want to roll back after
the tests, or creates a mess that you want to clean up safely

We will use the same approach as we did in the `test_class_example.py`
and generate a test class.

@author: hoelken
"""

from src.dojo import Hal9000
import unittest


class ExampleTest(unittest.TestCase):
    """
    Minimal test class demonstration with setup and teardown methods
    """

    def setUp(self):
        """
        The testing framework will automatically call this method __for__ every single test we run.
        When you do the same setup steps over and over again, you can DRY them out here.
        """
        self.hal = Hal9000('Dave')

    def tearDown(self):
        """
        The testing framework will automatically call this method __after__ every single test we run.
        This will happen regardless of the tests outcome! When you do made a mess, clean it up here!
        """
        # Yeah I know, this clean up is silly. However, it should give you an idea on how to use this method.
        self.hal = None

    def test_greetings(self):
        self.assertEqual(self.hal.greet(), 'Affirmative, Dave. I read you.')

    def test_self_test(self):
        self.assertEqual(self.hal.self_test(), {'AE35': 72 * 60 * 60})

    def test_pod_bay_doors(self):
        with self.assertRaises(RuntimeError):
            self.hal.open_pod_bay_doors()
