#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
# Minimal unit test example

In the minimal setup you don't need any test classes or
other setup. Just load the module(s) you want to test and
write a test method.

Methods starting with `test_` will be recognised and executed
by `pytest` automatically.

@author: hoelken
"""

from src.dojo import Hal9000


def test_greeting() -> None:
    """
    Minimal example on how to assert the return value of a method.
    In this example we test the branch where no user is set.
    """
    hal = Hal9000()
    assert hal.greet() == 'Hello world!'


def test_greeting_user() -> None:
    """
    Minimal example on how to assert the return value of a method.
    In this example we test the branch where a user is set.
    """
    hal = Hal9000(user='Dave')
    assert hal.greet() == 'Affirmative, Dave. I read you.'
